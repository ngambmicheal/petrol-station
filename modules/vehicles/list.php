<?php 

include('../../common/header.php'); 
include('../../common/connect.php');
?>

<div class="content-wrapper">

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Vehicle List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Vehicle List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
<div class="row">
<div class="col-12">
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Vehicles</h3>

    <div class="card-tools">
        <div class="input-group input-group-sm" style="width: 150px;">
        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

        <div class="input-group-append">
            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
        </div>
        </div>
    </div>
    </div>
    <!-- /.card-header -->

    <?php 
        $vehicles = mysqli_query($link, 'select * from vehicles');
    ?>
    <div class="card-body table-responsive p-0" style="height: 300px;">
      <table class="table table-head-fixed text-nowrap table-striped">
        <thead>
          <tr>
            <th>Matricule Id</th>
            <th>Brand</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
            <?php 
                while($row = mysqli_fetch_array($vehicles)){
                    echo "
                        <tr>
                            <td> {$row['matricule_id']} </td>
                            <td> {$row['brand']} </td>
                            <td> {$row['petrol_type']} </td>
                            <td> <a class='btn btn-primary btn-xs' href='/modules/vehicles/edit.php'><i class='fas fa-eye'></i> Edit </a> 
                                 <a class='btn btn-danger btn-xs' href='/modules/vehicles/delete.php?matricule_id={$row['matricule_id']}'> <i class='fas fa-trash'></i> Delete</a>
                            </td>
                        </tr>
                    ";
                }
            ?>
            <tr>

            </tr>

        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>